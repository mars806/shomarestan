package mr.bisi.myshomarestan;

import android.app.ProgressDialog;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;

import androidx.appcompat.app.AppCompatActivity;
import mr.bisi.myshomarestan.databinding.ActivityActivityVideoviewMainBinding;


public class TrainActivity extends AppCompatActivity {

    ActivityActivityVideoviewMainBinding binding;
    String VideoURL = "http://httppdida2018.ir/Klip/klipshmarstan.mp4";
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityActivityVideoviewMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("در حال بارگذاری ویدیو...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

        try {
            MediaController mediacontroller = new MediaController(this);
            mediacontroller.setAnchorView(binding.VideoView);
            Uri video = Uri.parse(this.VideoURL);
            binding.VideoView.setMediaController(mediacontroller);
            binding.VideoView.setVideoURI(video);

        } catch (Exception e) {
            Log.e("Error", e.toString());
            e.printStackTrace();
        }

        binding.VideoView.requestFocus();
        binding.VideoView.setOnPreparedListener(mp -> {
            pDialog.dismiss();
            binding.VideoView.start();
        });
    }
}
