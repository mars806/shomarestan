package mr.bisi.myshomarestan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CodeModel implements Serializable
{

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("id")
    @Expose
    private String id;
    private final static long serialVersionUID = 1009053506840240505L;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}