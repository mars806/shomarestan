package mr.bisi.myshomarestan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NumbersModel implements Serializable
{

    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("number")
    @Expose
    private String number;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("service")
    @Expose
    private String service;
    private final static long serialVersionUID = 3062273466542005618L;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

}