package mr.bisi.myshomarestan.utils;

public interface RecyclerViewClickListener {
    void onClick(int position, String countryID, String countryCode);
}