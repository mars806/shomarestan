package mr.bisi.myshomarestan.api;


import mr.bisi.myshomarestan.model.CodeModel;
import mr.bisi.myshomarestan.model.CountryModel;
import mr.bisi.myshomarestan.model.VerifyCodeModel;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIService {

    @GET("active.php?service=")
    Call<CountryModel> getCountries(@Query("service") String service);

    @FormUrlEncoded
    @POST("reg.php")
    Call<String> getCredit(@Field("id") String userId);

    @GET("price.php")
    Call<String> getPrice();

    @GET("time.php")
    Call<String> getTime(@Query("user")String userId);

    @GET("sts2.txt")
    Call<String> getCheckPay();

    @FormUrlEncoded
    @POST("shopnew.php")
    Call<String> recordBuyResult(@Field("user")String userId, @Field("record") String record, @Field("token") String token);

    @FormUrlEncoded
    @POST("orders.php")
    Call<String> getRecentlyNumbers(@Field("tname") String userId);

    @FormUrlEncoded
    @POST("get.php")
    Call<CodeModel> getCodeNumber(@Field("id") String userId, @Field("service")String service, @Field("status")String status, @Field("tzid")String tzid, @Field("ksh")String country);

    @FormUrlEncoded
    @POST("get.php")
    Call<VerifyCodeModel> getVerifyCode(@Field("id") String userId, @Field("service")String service, @Field("status")String status, @Field("tzid")String tzid, @Field("ksh")String country);

    @FormUrlEncoded
    @POST("insertnew.php")
    Call<String> insertInfo(@Field("userid")String userID, @Field("number")String number, @Field("code")String code, @Field("service")String service);
}
