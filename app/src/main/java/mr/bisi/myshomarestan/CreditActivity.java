package mr.bisi.myshomarestan;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import mr.bisi.myshomarestan.api.APIClient;
import mr.bisi.myshomarestan.api.APIService;
import mr.bisi.myshomarestan.databinding.ActivityCreditBinding;
import mr.bisi.myshomarestan.util.IabHelper;
import mr.bisi.myshomarestan.util.IabResult;
import mr.bisi.myshomarestan.util.Purchase;
import mr.bisi.myshomarestan.utils.Billing;
import mr.bisi.myshomarestan.utils.CreditViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CreditActivity extends AppCompatActivity {

    ActivityCreditBinding binding;
    ProgressDialog pDialog;
    APIService apiService;
    String userId;

    public String money = "0";
    private String TAG = "CreditActivity";
    Observer<String> creditObserver;
    CreditViewModel viewModel;

    String payMethod = "cafe";

    // (arbitrary) request code for the purchase flow
    static final int RC_REQUEST = 10001;


    // The helper object
    IabHelper mHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityCreditBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        viewModel = new  ViewModelProvider.NewInstanceFactory().create(CreditViewModel.class);
        apiService = APIClient.getRetrofitInstance().create(APIService.class);
        showProgressDialog();
        getCredit();

        creditObserver = newName -> {
            // Update the UI, in this case, a TextView.
            binding.txtToolbar.setText("اعتبار شما: " + newName + "  تومان");
        };
        viewModel.credit.observe(this, creditObserver);

        mHelper = new IabHelper(this, Billing.rsa_key);

        mHelper.enableDebugLogging(true, TAG);

        mHelper.startSetup(result -> {
            if (result.isSuccess())
                mHelper.queryInventoryAsync(mGotInventoryListener);
            else
                Toast.makeText(getApplicationContext(), "startSetup failed", Toast.LENGTH_LONG).show();
        });



        binding.btn5k.setOnClickListener(v ->{
            money = "5000";
            getPayMethod(Billing.ksu1_key);
        });

        binding.btn10k.setOnClickListener(v ->{
            money = "10000";
            getPayMethod(Billing.ksu2_key);
        });

        binding.btn20k.setOnClickListener(v ->{
            money = "20000";
            getPayMethod(Billing.ksu3_key);
        });

        binding.btn50k.setOnClickListener(v ->{
            money = "50000";
            getPayMethod(Billing.ksu4_key);
        });

        binding.btn100k.setOnClickListener(v -> {
            money = "100000";
            getPayMethod(Billing.ksu5_key);
        });

        binding.btn150k.setOnClickListener(v -> {
            money = "150000";
            getPayMethod(Billing.ksu6_key);
        });

        binding.btn300k.setOnClickListener(v -> {
            money = "300000";
            getPayMethod(Billing.ksu7_key);
        });

        binding.btn500k.setOnClickListener(v -> {
            money = "500000";
            getPayMethod(Billing.ksu8_key);
        });

        binding.button8.setOnClickListener(v -> onBackPressed());

    }

    private void getPayMethod(String key) {
        Call<String> call = apiService.getCheckPay();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {
                String check = response.body();
                Log.i(TAG, "onResponse: " + response.body());

                if (check != null) {
                    if (check.toLowerCase().equals("no"))
                        launchCafeBazarPurchaseFlow(key);
                    else
                        lunchZarinpal();
                }else
                    Toast.makeText(getApplicationContext(), "متد پرداخت نامشخص ...", Toast.LENGTH_LONG).show();

            }
            @Override
            public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {
                Toast.makeText(getApplicationContext(), "خطا در تعیین متد پرداخت ...", Toast.LENGTH_LONG).show();
                Log.e(TAG, "onFailure: " + t );
            }
        });
    }

    private void lunchZarinpal() {
        Intent intent  = new Intent(CreditActivity.this, ZarinpalActivity.class);
        intent.putExtra("money", money);
        startActivity(intent);
    }


    private void launchCafeBazarPurchaseFlow(String ksu){
        try {
            mHelper.launchPurchaseFlow(this, ksu, RC_REQUEST, this.mPurchaseFinishedListener);
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "در حال برقراری ارتباط با بازار ...", Toast.LENGTH_LONG).show();
        }
    }

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = (result, inventory) -> {
        Log.d(TAG, "mGotInventoryListener finished: " + result + ", inventory: " + inventory);
        hideProgressDialog();
    };

    // Callback for when a purchase is finished
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
            if (result.isSuccess())
                mHelper.consumeAsync(purchase, mConsumeFinishedListener);
        }
    };

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = (purchase, result) -> {
        Log.d(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);
        if (result.isSuccess()) {
            Log.d(TAG, "token: " + purchase.getToken()+ " getPackageName: " + purchase.getPackageName()
            + " getSku: " + purchase.getSku());
            recordBuyResult(purchase.getToken());
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);
        if (mHelper == null) return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        }
        else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }

    private void recordBuyResult(String token){
        Call<String> call = apiService.recordBuyResult(userId, money, token);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {
               if (response.isSuccessful()){
                   viewModel.getCredit();
               }
            }

            @Override
            public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {
                Toast.makeText(CreditActivity.this, "ثبت خرید ناموفق", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getCredit() {
        userId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        viewModel.initViewModel(apiService, userId);
        viewModel.getCredit();
    }

    private void showProgressDialog() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("لطفا کمی صبر کنید...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
    }

    private void hideProgressDialog(){
        pDialog.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) {
            mHelper.dispose();
            mHelper = null;
        }
    }

}
