package mr.bisi.myshomarestan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CountryModel implements Serializable
{

    @SerializedName("onlineRU")
    @Expose
    private String onlineRU;
//    @SerializedName("country")
//    @Expose
//    private String country;
    @SerializedName("onlineFL")
    @Expose
    private String onlineFL;
    @SerializedName("onlineUK")
    @Expose
    private String onlineUK;
    @SerializedName("onlineBR")
    @Expose
    private String onlineBR;
    @SerializedName("onlinePL")
    @Expose
    private String onlinePL;
    @SerializedName("onlineZA")
    @Expose
    private String onlineZA;
    @SerializedName("onlineEG")
    @Expose
    private String onlineEG;
    @SerializedName("onlinePY")
    @Expose
    private String onlinePY;
    @SerializedName("onlineUS")
    @Expose
    private String onlineUS;
    @SerializedName("onlineAR")
    @Expose
    private String onlineAR;
    @SerializedName("onlineKE")
    @Expose
    private String onlineKE;
    @SerializedName("onlineKG")
    @Expose
    private String onlineKG;
    @SerializedName("onlineNZ")
    @Expose
    private String onlineNZ;
    @SerializedName("onlineLV")
    @Expose
    private String onlineLV;
    @SerializedName("onlineCN2")
    @Expose
    private String onlineCN2;
    @SerializedName("onlineEE")
    @Expose
    private String onlineEE;
    @SerializedName("onlineFR")
    @Expose
    private String onlineFR;
    @SerializedName("onlineIL")
    @Expose
    private String onlineIL;
    @SerializedName("onlineRO")
    @Expose
    private String onlineRO;
    @SerializedName("onlineVN")
    @Expose
    private String onlineVN;
    @SerializedName("onlineID")
    @Expose
    private String onlineID;
    @SerializedName("onlineSP")
    @Expose
    private String onlineSP;
    @SerializedName("onlineDK")
    @Expose
    private String onlineDK;
    @SerializedName("onlineUA")
    @Expose
    private String onlineUA;
    @SerializedName("onlinePH")
    @Expose
    private String onlinePH;
    @SerializedName("onlineMY")
    @Expose
    private String onlineMY;
    @SerializedName("onlineLT")
    @Expose
    private String onlineLT;


    @SerializedName("onlineFI")
    @Expose
    private String onlineFI;

    @SerializedName("onlineKH")
    @Expose
    private String onlineKH;

    @SerializedName("onlineGE")
    @Expose
    private String onlineGE;

    @SerializedName("onlineES")
    @Expose
    private String onlineES;

    @SerializedName("onlineSE")
    @Expose
    private String onlineSE;

    @SerializedName("onlinePT")
    @Expose
    private String onlinePT;

    @SerializedName("onlineMX")
    @Expose
    private String onlineMX;

    @SerializedName("onlineDO")
    @Expose
    private String onlineDO;


    @SerializedName("onlineLA")
    @Expose
    private String onlineLA;


    private final static long serialVersionUID = 5249717070068773601L;

    public String getOnlineRU() {
        return onlineRU;
    }


    public void setOnlineRU(String onlineRU) {
        this.onlineRU = onlineRU;
    }

//    public String getCountry() {
//        return country;
//    }
//
//    public void setCountry(String country) {
//        this.country = country;
//    }

    public String getOnlineFL() {
        return onlineFL;
    }

    public void setOnlineFL(String onlineFL) {
        this.onlineFL = onlineFL;
    }

    public String getOnlineUK() {
        return onlineUK;
    }

    public void setOnlineUK(String onlineUK) {
        this.onlineUK = onlineUK;
    }

    public String getOnlineBR() {
        return onlineBR;
    }

    public void setOnlineBR(String onlineBR) {
        this.onlineBR = onlineBR;
    }

    public String getOnlinePL() {
        return onlinePL;
    }

    public void setOnlinePL(String onlinePL) {
        this.onlinePL = onlinePL;
    }

    public String getOnlineZA() {
        return onlineZA;
    }

    public void setOnlineZA(String onlineZA) {
        this.onlineZA = onlineZA;
    }

    public String getOnlineEG() {
        return onlineEG;
    }

    public void setOnlineEG(String onlineEG) {
        this.onlineEG = onlineEG;
    }

    public String getOnlinePY() {
        return onlinePY;
    }

    public void setOnlinePY(String onlinePY) {
        this.onlinePY = onlinePY;
    }

    public String getOnlineUS() {
        return onlineUS;
    }

    public void setOnlineUS(String onlineUS) {
        this.onlineUS = onlineUS;
    }

    public String getOnlineAR() {
        return onlineAR;
    }

    public void setOnlineAR(String onlineAR) {
        this.onlineAR = onlineAR;
    }

    public String getOnlineKE() {
        return onlineKE;
    }

    public void setOnlineKE(String onlineKE) {
        this.onlineKE = onlineKE;
    }

    public String getOnlineKG() {
        return onlineKG;
    }

    public void setOnlineKG(String onlineKG) {
        this.onlineKG = onlineKG;
    }

    public String getOnlineNZ() {
        return onlineNZ;
    }

    public void setOnlineNZ(String onlineNZ) {
        this.onlineNZ = onlineNZ;
    }

    public String getOnlineLV() {
        return onlineLV;
    }

    public void setOnlineLV(String onlineLV) {
        this.onlineLV = onlineLV;
    }

    public String getOnlineCN2() {
        return onlineCN2;
    }

    public void setOnlineCN2(String onlineCN2) {
        this.onlineCN2 = onlineCN2;
    }

    public String getOnlineEE() {
        return onlineEE;
    }

    public void setOnlineEE(String onlineEE) {
        this.onlineEE = onlineEE;
    }

    public String getOnlineFR() {
        return onlineFR;
    }

    public void setOnlineFR(String onlineFR) {
        this.onlineFR = onlineFR;
    }

    public String getOnlineIL() {
        return onlineIL;
    }

    public void setOnlineIL(String onlineIL) {
        this.onlineIL = onlineIL;
    }

    public String getOnlineRO() {
        return onlineRO;
    }

    public void setOnlineRO(String onlineRO) {
        this.onlineRO = onlineRO;
    }

    public String getOnlineVN() {
        return onlineVN;
    }

    public void setOnlineVN(String onlineVN) {
        this.onlineVN = onlineVN;
    }

    public String getOnlineID() {
        return onlineID;
    }

    public void setOnlineID(String onlineID) {
        this.onlineID = onlineID;
    }

    public String getOnlineSP() {
        return onlineSP;
    }

    public void setOnlineSP(String onlineSP) {
        this.onlineSP = onlineSP;
    }

    public String getOnlineDK() {
        return onlineDK;
    }

    public void setOnlineDK(String onlineDK) {
        this.onlineDK = onlineDK;
    }

    public String getOnlineUA() {
        return onlineUA;
    }

    public void setOnlineUA(String onlineUA) {
        this.onlineUA = onlineUA;
    }

    public String getOnlinePH() {
        return onlinePH;
    }

    public void setOnlinePH(String onlinePH) {
        this.onlinePH = onlinePH;
    }

    public String getOnlineMY() {
        return onlineMY;
    }

    public void setOnlineMY(String onlineMY) {
        this.onlineMY = onlineMY;
    }

    public String getOnlineLT() {
        return onlineLT;
    }

    public void setOnlineLT(String onlineLT) {
        this.onlineLT = onlineLT;
    }

    public String getOnlineFI() {
        return onlineFI;
    }

    public void setOnlineFI(String onlineFI) {
        this.onlineFI = onlineFI;
    }

    public String getOnlineKH() {
        return onlineKH;
    }

    public void setOnlineKH(String onlineKH) {
        this.onlineKH = onlineKH;
    }

    public String getOnlineGE() {
        return onlineGE;
    }

    public void setOnlineGE(String onlineGE) {
        this.onlineGE = onlineGE;
    }

    public String getOnlineES() {
        return onlineES;
    }

    public void setOnlineES(String onlineES) {
        this.onlineES = onlineES;
    }

    public String getOnlineSE() {
        return onlineSE;
    }

    public void setOnlineSE(String onlineSE) {
        this.onlineSE = onlineSE;
    }

    public String getOnlinePT() {
        return onlinePT;
    }

    public void setOnlinePT(String onlinePT) {
        this.onlinePT = onlinePT;
    }

    public String getOnlineMX() {
        return onlineMX;
    }

    public void setOnlineMX(String onlineMX) {
        this.onlineMX = onlineMX;
    }

    public String getOnlineDO() {
        return onlineDO;
    }

    public void setOnlineDO(String onlineDO) {
        this.onlineDO = onlineDO;
    }

    public String getOnlineLA() {
        return onlineLA;
    }

    public void setOnlineLA(String onlineLA) {
        this.onlineLA = onlineLA;
    }
}