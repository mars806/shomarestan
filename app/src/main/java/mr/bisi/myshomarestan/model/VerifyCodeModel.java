package mr.bisi.myshomarestan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class VerifyCodeModel implements Serializable
{

    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("text")
    @Expose
    private String text;
    private final static long serialVersionUID = 7172681156473978768L;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}