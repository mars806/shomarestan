package mr.bisi.myshomarestan.model;

public class BuyNumberModel {
    private String info;
    private String pay;
    private String onlineCode;

    public BuyNumberModel(String info, String pay, String onlineCode) {
        this.info = info;
        this.pay = pay;
        this.onlineCode = onlineCode;
    }

    public String getInfo() {
        return info;
    }

    public String getPay() {
        return pay;
    }

    public String getOnlineCode() {
        return onlineCode;
    }
}
