package mr.bisi.myshomarestan;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.NotNull;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import mr.bisi.myshomarestan.adapter.BuyNumberAdapter;
import mr.bisi.myshomarestan.utils.RecyclerViewClickListener;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * get list of cities from inner database by database handler
 */
public class BuyNumberDialog extends DialogFragment implements RecyclerViewClickListener {

    private BuyNumberActivity activity;
    private String opt, countryId, countryCode;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setStyle(DialogFragment.STYLE_NORMAL,
                android.R.style.Theme_DeviceDefault_Dialog_NoActionBar_MinWidth);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_buy_number, container, false);


        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);

        activity = (BuyNumberActivity) getActivity();

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        BuyNumberAdapter adapter = new BuyNumberAdapter(activity.list, this, activity.opt);
        recyclerView.setAdapter(adapter);

        return view;

    }

    @Override
    public void onClick(int position, String countryId, String countryCode) {
        Log.i("BuyNumberDialog", "onClick: money: " + activity.money + " pay: " + activity.list.get(position).getPay());
        if (Integer.parseInt(activity.money) >= Integer.parseInt(activity.list.get(position).getPay())) {
            this.countryCode = countryCode;
            this.countryId = countryId;
            this.opt = activity.opt;
            validationTime();

        } else {
            new AlertDialog.Builder(activity)
                    .setMessage("اعتبار شما برای خرید این سرویس کافی نیست\nلطفا ابتدا اعتبار خود را افزایش دهید")
                    .setNeutralButton("باشه", (dialog, which) -> {
                        startActivity(new Intent(activity, CreditActivity.class));
                        dialog.dismiss();
                        activity.finish();
                    }).show();
        }

    }

    private void validationTime() {
        Call<String> call  = activity.apiService.getTime(activity.userId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {
                if (response.isSuccessful()) {
                    int time = 0;
                    try {
                        if (response.body() != null) {
                            time = Integer.parseInt(response.body());
                        }
                    } catch (Exception e) {
                        Log.i("onResponse", "onResponse: " + response.body());
                    }

                    Log.i("onResponse", "onResponse: " + response.body());

                    if (time == 0) {
                        Intent intent = new Intent(activity, MainActivity.class);
                        intent.putExtra("opt", opt);
                        intent.putExtra("countryId", countryId);
                        intent.putExtra("CountryCode", countryCode);
                        intent.putExtra("title", activity.title);
                        startActivity(intent);
                        activity.finish();
                    } else {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
                        dialog.setCancelable(true);
                        dialog.setTitle("محدودیت زمانی");
                        dialog.setMessage("زمان باقیمانده تا فعال سازی مجدد برنامه برای شما:\n" + response.body() + " دقیقه\n\nاین محدودیت توسط سرور میباشد و تا زمان اتمام صبر کنید سپس مجددا میتوانید شماره دریافت کنید ");
                        dialog.setPositiveButton("باشه", (dialog1, which) -> {
                        });
                        dialog.show();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {

            }
        });
    }
}
