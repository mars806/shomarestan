package mr.bisi.myshomarestan.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import mr.bisi.myshomarestan.R;
import mr.bisi.myshomarestan.model.BuyNumberModel;
import mr.bisi.myshomarestan.utils.RecyclerViewClickListener;


public class BuyNumberAdapter extends RecyclerView.Adapter<BuyNumberAdapter.MyViewHolder> {

    private List<BuyNumberModel> list;
    private RecyclerViewClickListener listener;
    private String messenger;

    public enum Environment {
        RU("روسیه", "🇷🇺", "+7"),
//        NZ("نیوزلند" ,"🇦🇺", "+64"),
        CN2("چین", "🇨🇳", "+86"),
        EE("استونی", "🇪🇪", "+372"),
        FR("فرانسه" , "🇫🇷", "+33"),
        US("آمریکا" ,"🇺🇸", "+1"),
        PY("پاراگوئه", "🇵🇾", "+595"),
        AR("آرژانتین", "🇦🇷", "+54"),
        KE("کنیا","🇰🇪", "+254"),
        MY("مالزی", "\uD83C\uDDF2\uD83C\uDDFE", "+60"),
        IL("اسرائیل","🇮🇱", "+972"),//---------------------------------------
//        FI("فنلاند","\uD83C\uDDEB\uD83C\uDDEE", "+358"),
//        KH("کامبوج", "\uD83C\uDDF0\uD83C\uDDED", "+855"),
//        GE("جورجیا","\uD83C\uDDEC\uD83C\uDDEA", "+9"),
//        ES("اسپانیا","\uD83C\uDDEA\uD83C\uDDF8", "+34"),
//        SE("سوئد", "\uD83C\uDDF8\uD83C\uDDEA", "+46"),
//        PT("پرتغال","\uD83C\uDDF5\uD83C\uDDF9", "+351"),
//        MX("مکزیک","\uD83C\uDDF2\uD83C\uDDFD", "+52"),
//        DO("دومنیکن","\uD83C\uDDE9\uD83C\uDDF4", "+1"),
//        LT("لیتوانی","\uD83C\uDDF1\uD83C\uDDF9", "+370"),
        LA("لائوس", "\uD83C\uDDF1\uD83C\uDDE6", "+8"),//---------------------------------------------
        VN("ویتنام","🇻🇳", "+84"),
        DK("دانمارک","🇩🇰", "+45"),
        UK("انگلیس", "🇬🇧", "+44"),
        UA("اکراین","🇺🇦", "+380"),
        ID("اندونزی","🇮🇩", "+62"),
        PH("فیلیپین","🇵🇭", "+63"),
        BR("برزیل", "\uD83C\uDDE7\uD83C\uDDF7", "+55"),
        EG("مصر", "\uD83C\uDDEA\uD83C\uDDEC", "+20"),
        ZA("افریقا", "\uD83C\uDDFF\uD83C\uDDE6", "+24"),
        PL("لهستان", "\uD83C\uDDF5\uD83C\uDDF1", "+48");

        private String countryName;
        private String flag;
        private String countryCode;

        Environment(String flag, String countryName, String countryCode) {
            this.countryName = countryName;
            this.flag = flag;
            this.countryCode = countryCode;
        }

    }

    public BuyNumberAdapter(List<BuyNumberModel> list, RecyclerViewClickListener listener, String messenger){
        this.list = list;
        this.listener = listener;
        this.messenger = messenger;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_buy_number_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        BuyNumberModel model = list.get(position);
        if (messenger.equals("opt29")) {
            if (
                    model.getOnlineCode().equals(Environment.RU.name())||
                            model.getOnlineCode().equals(Environment.CN2.name())||
                            model.getOnlineCode().equals(Environment.VN.name())||
                            model.getOnlineCode().equals(Environment.UA.name())||
                            model.getOnlineCode().equals(Environment.PH.name())||
                            model.getOnlineCode().equals(Environment.ID.name())
//
//                            Environment.values()[position].countryCode.equals("+86")||
//                            Environment.values()[position].countryCode.equals("+84")||
//                            Environment.values()[position].countryCode.equals("+380")||
//                            Environment.values()[position].countryCode.equals("+63")||
//                    Environment.values()[position].countryCode.equals("+62")
            ) {

                holder.txtCountry.setText("کشور صدور شماره: " + Environment.valueOf(model.getOnlineCode()).flag
                        + Environment.valueOf(model.getOnlineCode()).countryName +
                        "\n" + model.getInfo() + "\n*خط های این کشور نیاز به رفع ریپورت دارند*");
            }else
                holder.txtCountry.setText("کشور صدور شماره: "+ Environment.valueOf(model.getOnlineCode()).flag
                        + Environment.valueOf(model.getOnlineCode()).countryName +
                        "\n" + model.getInfo());
        }else
            holder.txtCountry.setText("کشور صدور شماره: "+  Environment.valueOf(model.getOnlineCode()).flag
                    + Environment.valueOf(model.getOnlineCode()).countryName +
                    "\n" + model.getInfo());

        holder.txtCountry.setOnClickListener(v ->
                listener.onClick(
                        position,
//                        Environment.values()[position].name(),
//                        Environment.values()[position].countryCode


                        Environment.valueOf(model.getOnlineCode()).name(),
                        Environment.valueOf(model.getOnlineCode()).countryCode
                ));
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView txtCountry;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtCountry = itemView.findViewById(R.id.txtCountry);
        }
    }

}
