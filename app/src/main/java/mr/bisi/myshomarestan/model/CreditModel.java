package mr.bisi.myshomarestan.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CreditModel implements Serializable
{

    @SerializedName("coin")
    @Expose
    private String coin;
    @SerializedName("nazar")
    @Expose
    private String nazar;
    private final static long serialVersionUID = -4092530884862225436L;

    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin;
    }

    public String getNazar() {
        return nazar;
    }

    public void setNazar(String nazar) {
        this.nazar = nazar;
    }

}