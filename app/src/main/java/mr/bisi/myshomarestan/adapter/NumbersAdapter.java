package mr.bisi.myshomarestan.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import mr.bisi.myshomarestan.R;
import mr.bisi.myshomarestan.model.NumbersModel;

public class NumbersAdapter extends RecyclerView.Adapter<NumbersAdapter.MyViewHolder> {

    private List<NumbersModel> list;

    public NumbersAdapter(List<NumbersModel> list){
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_number_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        NumbersModel model = list.get(position);
        holder.textView.setText("نوع سرویس: شماره مجازی " + model.getService() + " \nشماره: " + model.getNumber() + " \nکد فعال سازی: " + model.getCode());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
        }
    }

}
