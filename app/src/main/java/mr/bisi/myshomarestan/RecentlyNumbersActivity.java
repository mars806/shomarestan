package mr.bisi.myshomarestan;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import mr.bisi.myshomarestan.adapter.NumbersAdapter;
import mr.bisi.myshomarestan.api.APIClient;
import mr.bisi.myshomarestan.api.APIService;
import mr.bisi.myshomarestan.databinding.ActivityOrdersBinding;
import mr.bisi.myshomarestan.model.NumbersModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecentlyNumbersActivity extends AppCompatActivity {

    ActivityOrdersBinding binding;
    private String userId;
    private APIService apiService;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOrdersBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        showProgressDialog();
        apiService  = APIClient.getRetrofitInstance().create(APIService.class);
        getCredit();
    }


    private void getCredit() {
        userId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        getPhoneNumbers();
    }

//    private void getPermission() {
//        Dexter.withContext(this)
//                .withPermissions(Manifest.permission.READ_PHONE_STATE)
//                .withListener(new MultiplePermissionsListener() {
//                    @Override
//                    public void onPermissionsChecked(MultiplePermissionsReport report) {
//                        if (report.areAllPermissionsGranted()) {
//                            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//                            userId = tm.getDeviceId();
//                            getPhoneNumbers();
//                        } else {
//                            onBackPressed();
//                        }
//                        if (report.isAnyPermissionPermanentlyDenied()) {
//                            ActivityCompat.requestPermissions(RecentlyNumbersActivity.this,
//                                    new String[]{Manifest.permission.READ_PHONE_STATE}, 1052);
//                        }
//                    }
//
//                    @Override
//                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//                        token.continuePermissionRequest();
//                    }
//
//                }).check();
//    }


    private void getPhoneNumbers(){
        Call<String> call = apiService.getRecentlyNumbers(userId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {

                Log.i("RecentlyNumbersActivity", "onResponse: " + response.body());
                hideProgressDialog();

                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();

                List<NumbersModel> modelList = gson.fromJson(response.body(), new TypeToken<List<NumbersModel>>() {
                }.getType());

                if (modelList != null) {
                    if (modelList.size() == 0)
                        showAlertDialog();
                    else{
                        final LinearLayoutManager layoutManager = new LinearLayoutManager(RecentlyNumbersActivity.this);
                        binding.recyclerView.setLayoutManager(layoutManager);
                        NumbersAdapter adapter = new NumbersAdapter(modelList);
                        binding.recyclerView.setAdapter(adapter);
                    }
                }else
                    showAlertDialog();

            }

            @Override
            public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {
                hideProgressDialog();
            }
        });
    }

    private void showAlertDialog() {
        AlertDialog.Builder dialog2 = new AlertDialog.Builder(RecentlyNumbersActivity.this);
        dialog2.setTitle("استعلام سرور");
        dialog2.setMessage("شما هیچ شماره ای تاکنون نداشته اید\nبرای ثبت شماره گزینه خرید شماره را انتخاب کنید");
        dialog2.setPositiveButton("خرید شماره جدید", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(RecentlyNumbersActivity.this, BuyNumberActivity.class));
                finish();
            }
        });
        dialog2.show();
    }

    private void showProgressDialog() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("لطفا کمی صبر کنید...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
    }

    private void hideProgressDialog(){
        pDialog.dismiss();
    }
}
