package mr.bisi.myshomarestan;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

public class Welcome extends AppCompatActivity {
    WebView webView;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        initWebView();

        new Handler().postDelayed(() -> {
            if (!isConnectedToInternet())
                createAlertDialog();
            else if (isPackageInstalled("com.farsitel.bazaar")) {
                startActivity(new Intent(Welcome.this, ListActivity.class));
                finish();

            } else
                installBazarDialog();
        }, 4000);
    }

    private void initWebView(){
        webView =  findViewById(R.id.webView1);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebViewClient(new WebViewClient());
        webView.setBackgroundColor(0);
        webView.loadUrl("file:///android_asset/welcome.html");
    }

    public boolean isConnectedToInternet() {
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        //we are connected to a network
        return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED;
    }

    public boolean isPackageInstalled(String PackageName) {
        try {
            getPackageManager().getPackageInfo(PackageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }

    private void createAlertDialog(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(Welcome.this);
        dialog.setCancelable(false);
        dialog.setTitle("خطا اتصال به اینترنت");
        dialog.setMessage("دستگاه شما به اینترنت متصل نیست.لطفا اینترنت گوشی را فعال کنید و مجددا برنامه را باز کنید...");
        dialog.setPositiveButton("باشه", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.show();
    }

    private void installBazarDialog(){
        AlertDialog.Builder dialog2 = new AlertDialog.Builder(Welcome.this);
        dialog2.setCancelable(false);
        dialog2.setMessage("برای اجرای این برنامه باید کافه بازار را روی گوشی خود نصب کنید\nاین کار بسیار سریع و اسان است\n\nنصب کنیم؟");
        dialog2.setPositiveButton("اره،نصب میکنم", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
               startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://www.cafebazaar.ir")));
            }
        });
        dialog2.setNegativeButton("نه بیخیال", (dialog, which) -> System.exit(0));
        dialog2.show();
    }
}