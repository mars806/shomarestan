package mr.bisi.myshomarestan;

import android.app.AlertDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;

import com.pushpole.sdk.PushPole;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import mr.bisi.myshomarestan.api.APIClient;
import mr.bisi.myshomarestan.api.APIService;
import mr.bisi.myshomarestan.databinding.ActivityListBinding;
import mr.bisi.myshomarestan.utils.CreditViewModel;


public class ListActivity extends AppCompatActivity {

    ActivityListBinding binding;

    String userId;
    private APIService apiService;

    CreditViewModel viewModel;
    Observer<String> creditObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityListBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        PushPole.initialize(this, true);

        viewModel = new  ViewModelProvider.NewInstanceFactory().create(CreditViewModel.class);
        apiService = APIClient.getRetrofitInstance().create(APIService.class);
        creditObserver = newName -> {
            // Update the UI, in this case, a TextView.
            binding.txtToolbar.setText("اعتبار شما: " + newName + "  تومان");
        };
        viewModel.credit.observe(this, creditObserver);


        binding.btnCredit.setOnClickListener(v ->
                startActivity(new Intent(this, CreditActivity.class)));

        binding.btnBuyNumber.setOnClickListener(v ->
                startActivity(new Intent(this, BuyNumberActivity.class)));

        binding.btnRecently.setOnClickListener(v ->
                startActivity(new Intent(this, RecentlyNumbersActivity.class)));

        binding.btnVideo.setOnClickListener(v ->
                startActivity(new Intent(this, TrainActivity.class)));

        binding.btnQ.setOnClickListener(v -> {
            AlertDialog.Builder dialog = new AlertDialog.Builder(ListActivity.this);
            dialog.setTitle("سوالات متداول");
            dialog.setMessage(R.string.about);
            dialog.setPositiveButton("فهمیدم", (dialog1, which) -> {
            });
            dialog.show();
        });

        binding.btnTelegram.setOnClickListener(v ->
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("https://t.me/shmark1"))));
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCredit();
    }

    private void getCredit() {
        userId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        binding.txtId.setText(userId);
        viewModel.initViewModel(apiService, userId);
        viewModel.getCredit();
    }
//
//        Dexter.withContext(this)
//                .withPermissions(Manifest.permission.READ_PHONE_STATE)
//                .withListener(new MultiplePermissionsListener() {
//                    @Override
//                    public void onPermissionsChecked(MultiplePermissionsReport report) {
//                        if (report.areAllPermissionsGranted()) {
//                            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//
//
//                        } else {
//                            onBackPressed();
//                        }
//                        if (report.isAnyPermissionPermanentlyDenied()) {
//                            ActivityCompat.requestPermissions(ListActivity.this,
//                                    new String[]{Manifest.permission.READ_PHONE_STATE}, 1052);
//                        }
//                    }
//
//                    @Override
//                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//                        token.continuePermissionRequest();
//                    }
//
//                }).check();


}
