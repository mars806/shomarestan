package mr.bisi.myshomarestan;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import mr.bisi.myshomarestan.api.APIClient;
import mr.bisi.myshomarestan.api.APIService;
import mr.bisi.myshomarestan.databinding.ActivityBuyNumberBinding;
import mr.bisi.myshomarestan.model.BuyNumberModel;
import mr.bisi.myshomarestan.model.CountryModel;
import mr.bisi.myshomarestan.model.PriceModel;
import mr.bisi.myshomarestan.utils.CreditViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuyNumberActivity extends AppCompatActivity {

    String TAG = "shomarestan";

    ActivityBuyNumberBinding binding;
    private ProgressDialog pDialog;
    String userId;
    public String money;
    public String title;

    public APIService apiService;
    CreditViewModel viewModel;
    Observer<String> creditObserver;
    private PriceModel priceModel;
    private CountryModel countryModel;

    List<BuyNumberModel> list;
    String opt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityBuyNumberBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        viewModel = new  ViewModelProvider.NewInstanceFactory().create(CreditViewModel.class);
        apiService = APIClient.getRetrofitInstance().create(APIService.class);
        getCredit();

        creditObserver = newName -> {
            // Update the UI, in this case, a TextView.
            money = newName;
            binding.txtToolbar.setText("اعتبار شما: " + newName + "  تومان");
        };
        viewModel.credit.observe(this, creditObserver);

        binding.btnTelegram.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt29";
            title = binding.btnTelegram.getText().toString();
            getPrice();
        });

        binding.btnInstagram.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt16";
            title = binding.btnInstagram.getText().toString();
            getPrice();
        });

        binding.btnWhatsApp.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt20";
            title = binding.btnWhatsApp.getText().toString();
            getPrice();
        });

        binding.btnViber.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt11";
            title =  binding.btnViber.getText().toString();
            getPrice();
        });

        binding.btnGmail.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt1";
            title = binding.btnGmail.getText().toString();
            getPrice();
        });

        binding.btnFacebook.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt2";
            title = binding.btnFacebook.getText().toString();
            getPrice();
        });

        binding.btnYahoo.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt65";
            title = binding.btnYahoo.getText().toString();
            getPrice();
        });

        binding.btnWeChat.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt67";
            title = binding.btnWeChat.getText().toString();
            getPrice();
        });

        binding.BtnSteam.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt58";
            title = binding.BtnSteam.getText().toString();
            getPrice();
        });

        binding.btnTwitter.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt41";
            title = binding.btnTwitter.getText().toString();
            getPrice();
        });



        binding.btnLinkedIn.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt8";
            title = binding.btnLinkedIn.getText().toString();
            getPrice();
        });

        binding.btnTikTok.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt104";
            title = binding.btnTikTok.getText().toString();
            getPrice();
        });

        binding.btnSnapChat.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt90";
            title = binding.btnSnapChat.getText().toString();
            getPrice();
        });

        binding.btnImo.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt111";
            title = binding.btnImo.getText().toString();
            getPrice();
        });

        binding.btnPayPal.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt83";
            title = binding.btnPayPal.getText().toString();
            getPrice();
        });

        binding.btnCoinBase.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt112";
            title = binding.btnCoinBase.getText().toString();
            getPrice();
        });

        binding.btnLine.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt37";
            title = binding.btnLine.getText().toString();
            getPrice();
        });




        binding.btnOther.setOnClickListener(v -> {
            showProgressDialog();
            opt = "opt19";
            title = binding.btnOther.getText().toString();
            getPrice();
        });

    }

    private void getCredit() {
        userId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        viewModel.initViewModel(apiService, userId);
        viewModel.getCredit();
    }

//    private void getPermission() {
//        Dexter.withContext(this)
//                .withPermissions(Manifest.permission.READ_PHONE_STATE)
//                .withListener(new MultiplePermissionsListener() {
//                    @Override
//                    public void onPermissionsChecked(MultiplePermissionsReport report) {
//                        if (report.areAllPermissionsGranted()) {
//                            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//                            userId = tm.getDeviceId();
//
//                            viewModel.initViewModel(apiService, userId);
//                            viewModel.getCredit();
//
//
//                        } else {
//                            onBackPressed();
//                        }
//                        if (report.isAnyPermissionPermanentlyDenied()) {
//                            ActivityCompat.requestPermissions(BuyNumberActivity.this,
//                                    new String[]{Manifest.permission.READ_PHONE_STATE}, 1052);
//                        }
//                    }
//
//                    @Override
//                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
//                        token.continuePermissionRequest();
//                    }
//
//                }).check();
//    }

    private void showProgressDialog() {
        pDialog = new ProgressDialog(this);
        pDialog.setMessage("لطفا کمی صبر کنید...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(true);
        pDialog.show();
    }

    private void hideProgressDialog(){
        pDialog.dismiss();
    }

    private void getPrice(){
        Call<String> call = apiService.getPrice();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONArray jsonArray = new JSONArray(response.body());
                        Object object = jsonArray.get(0);

                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();

                        PriceModel model = gson.fromJson(object.toString(), PriceModel.class);


                        if (model != null) {
                            priceModel = model;
                            getNumberList(opt);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.i(TAG, "onResponse: catch: " + e.toString());
                        Toast.makeText(BuyNumberActivity.this, "catch", Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {
                Log.i(TAG, "onFailure: getPrice" + t.toString());

            }
        });
    }

    private void getNumberList(String serviceId) {
        Call<CountryModel> call = apiService.getCountries(serviceId);
        call.enqueue(new Callback<CountryModel>() {
            @Override
            public void onResponse(@NotNull Call<CountryModel> call, @NotNull Response<CountryModel> response) {
                if (response.isSuccessful()){
                    CountryModel model = response.body();

                    if (model != null){
                        countryModel = model;
                        createCountriesDialogFragment();
                    }
                }
            }

            @Override
            public void onFailure(@NotNull Call<CountryModel> call, @NotNull Throwable t) {
                Log.i(TAG, "onFailure: getCountryList" + t.toString());
            }
        });

    }

    private void createCountriesDialogFragment() {
        list = createAdapterList();
        FragmentManager fragmentManager = getSupportFragmentManager();
        BuyNumberDialog dialog = new BuyNumberDialog();
        dialog.show(fragmentManager, "buy_number");

        hideProgressDialog();
    }

    private List<BuyNumberModel> createAdapterList() {

        List<BuyNumberModel> list = new ArrayList<>();
        if (countryModel.getOnlineRU() != null && !countryModel.getOnlineRU().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInforu(), priceModel.getPayru(), "RU"));
//        if (countryModel.getOnlineSP() != null && !countryModel.getOnlineSP().equals("null"))
//            list.add(new BuyNumberModel(priceModel.getInfosp(), priceModel.getPaysp(), "SP"));
        if (countryModel.getOnlineCN2() != null && !countryModel.getOnlineCN2().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfocn2(), priceModel.getPaycn2(), "CN2"));
        if (countryModel.getOnlineEE() != null && !countryModel.getOnlineEE().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfoee(), priceModel.getPayee(), "EE"));
        if (countryModel.getOnlineFR() != null && !countryModel.getOnlineFR().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfofr(), priceModel.getPayfr(), "FR"));
        if (countryModel.getOnlineUS() != null && !countryModel.getOnlineUS().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfous(), priceModel.getPayus(), "US"));
        if (countryModel.getOnlinePY() != null && !countryModel.getOnlinePY().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfopy(), priceModel.getPaypy(), "PY"));
        if (countryModel.getOnlineAR() != null && !countryModel.getOnlineAR().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfoar(), priceModel.getPayar(), "AR"));
        if (countryModel.getOnlineKE() != null && !countryModel.getOnlineKE().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfoke(), priceModel.getPayke(), "KE"));
        if (countryModel.getOnlineMY() != null && !countryModel.getOnlineMY().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfomy(), priceModel.getPaymy(), "MY"));
        if (countryModel.getOnlineIL() != null && !countryModel.getOnlineIL().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfoil(), priceModel.getPayil(), "IL"));



//        if (countryModel.getOnlineFI() != null && !countryModel.getOnlineFI().equals("null"))
//            list.add(new BuyNumberModel(priceModel.getInfofi(), priceModel.getPayfi(), "FI"));
//        if (countryModel.getOnlineKH() != null &&  !countryModel.getOnlineKH().equals("null"))
//            list.add(new BuyNumberModel(priceModel.getInfokh(), priceModel.getPaykh(), "KH"));
//        if (countryModel.getOnlineGE() != null &&  !countryModel.getOnlineGE().equals("null"))
//            list.add(new BuyNumberModel(priceModel.getInfoge(), priceModel.getPayge(), "GE"));
//        if (countryModel.getOnlineES() != null &&  !countryModel.getOnlineES().equals("null"))
//            list.add(new BuyNumberModel(priceModel.getInfoes(), priceModel.getPayes(), "ES"));
//        if (countryModel.getOnlineSE() != null &&  !countryModel.getOnlineSE().equals("null"))
//            list.add(new BuyNumberModel(priceModel.getInfose(), priceModel.getPayse(), "SE"));
//        if (countryModel.getOnlinePT() != null &&  !countryModel.getOnlinePT().equals("null"))
//            list.add(new BuyNumberModel(priceModel.getInfopt(), priceModel.getPaypt(), "PT"));
//        if (countryModel.getOnlineMX() != null &&  !countryModel.getOnlineMX().equals("null"))
//            list.add(new BuyNumberModel(priceModel.getInfomx(), priceModel.getPaymx(), "MX"));
//        if (countryModel.getOnlineDO() != null &&  !countryModel.getOnlineDO().equals("null"))
//            list.add(new BuyNumberModel(priceModel.getInfodo(), priceModel.getPaydo(), "DO"));
//        if (countryModel.getOnlineLT() != null &&  !countryModel.getOnlineLT().equals("null"))
//            list.add(new BuyNumberModel(priceModel.getInfolt(), priceModel.getPaylt(), "LT"));



        if (countryModel.getOnlineLA() != null && !countryModel.getOnlineLA().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfola(), priceModel.getPayla(), "LA"));
        if (countryModel.getOnlineVN() != null && !countryModel.getOnlineVN().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfovn(), priceModel.getPayvn(), "VN"));
        if (countryModel.getOnlineDK() != null && !countryModel.getOnlineDK().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfodk(), priceModel.getPaydk(), "DK"));
        if (countryModel.getOnlineUK() != null && !countryModel.getOnlineUK().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfouk(), priceModel.getPayuk(), "UK"));
        if (countryModel.getOnlineUA() != null && !countryModel.getOnlineUA().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfoua(), priceModel.getPayua(), "UA"));
        if (countryModel.getOnlineID() != null && !countryModel.getOnlineID().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfoid(), priceModel.getPayid(), "ID"));
        if (countryModel.getOnlinePH() != null && !countryModel.getOnlinePH().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfoph(), priceModel.getPayph(), "PH"));
        if (countryModel.getOnlineBR() != null && !countryModel.getOnlineBR().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfobr(), priceModel.getPaybr(), "BR"));
        if (countryModel.getOnlineEG() != null && !countryModel.getOnlineEG().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfoeg(), priceModel.getPayeg(), "EG"));
        if (countryModel.getOnlineZA() != null && !countryModel.getOnlineZA().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfoza(), priceModel.getPayza(), "ZA"));
        if (countryModel.getOnlinePL() != null &&  !countryModel.getOnlinePL().equals("null"))
            list.add(new BuyNumberModel(priceModel.getInfopl(), priceModel.getPaypl(), "PL"));


        return list;
    }

}
