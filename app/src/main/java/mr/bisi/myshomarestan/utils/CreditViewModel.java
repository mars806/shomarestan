package mr.bisi.myshomarestan.utils;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import mr.bisi.myshomarestan.api.APIService;
import mr.bisi.myshomarestan.model.CreditModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreditViewModel extends ViewModel {

    private APIService apiService;
    private String userId;
    public MutableLiveData<String> credit = new MutableLiveData<>();


    public void initViewModel(APIService apiService, String userId) {
        this.apiService = apiService;
        this.userId = userId;
    }

    public void getCredit() {
        Log.i("CreditModel", "getCredit: userId: " + userId);

        Call<String> call = apiService.getCredit(userId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {

                Log.i("CreditModel", "onResponse: " + response.body());

                JSONArray jsonArray;
                try {
                    jsonArray = new JSONArray(response.body());

                    Object object = jsonArray.get(0);

                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();

                    CreditModel model = gson.fromJson(object.toString(), CreditModel.class);

                    if (model != null)
                        credit.postValue(model.getCoin());
                    else
                        Log.i("CreditModel", "onResponse: model is null" + response.body());

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {
                Log.i("CreditModel", "Throwable: " + t);

            }
        });
    }
}
