package mr.bisi.myshomarestan.model;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PriceModel implements Serializable
{

    @SerializedName("inforu")
    @Expose
    private String inforu;
    @SerializedName("payru")
    @Expose
    private String payru;

    @SerializedName("infofi")
    @Expose
    private String infofi;
    @SerializedName("payfi")
    @Expose
    private String payfi;

    @SerializedName("infokh")
    @Expose
    private String infokh;
    @SerializedName("paykh")
    @Expose
    private String paykh;

    @SerializedName("infoge")
    @Expose
    private String infoge;
    @SerializedName("payge")
    @Expose
    private String payge;

    @SerializedName("infoes")
    @Expose
    private String infoes;
    @SerializedName("payes")
    @Expose
    private String payes;

    @SerializedName("infose")
    @Expose
    private String infose;
    @SerializedName("payse")
    @Expose
    private String payse;

    @SerializedName("infopt")
    @Expose
    private String infopt;
    @SerializedName("paypt")
    @Expose
    private String paypt;

    @SerializedName("infomx")
    @Expose
    private String infomx;
    @SerializedName("paymx")
    @Expose
    private String paymx;

    @SerializedName("infodo")
    @Expose
    private String infodo;
    @SerializedName("paydo")
    @Expose
    private String paydo;

    @SerializedName("infolt")
    @Expose
    private String infolt;
    @SerializedName("paylt")
    @Expose
    private String paylt;

    @SerializedName("infouk")
    @Expose
    private String infouk;
    @SerializedName("payuk")
    @Expose
    private String payuk;
    @SerializedName("infosp")
    @Expose
    private String infosp;
    @SerializedName("paysp")
    @Expose
    private String paysp;
    @SerializedName("infodk")
    @Expose
    private String infodk;
    @SerializedName("paydk")
    @Expose
    private String paydk;
    @SerializedName("infoua")
    @Expose
    private String infoua;
    @SerializedName("payua")
    @Expose
    private String payua;
    @SerializedName("infoph")
    @Expose
    private String infoph;
    @SerializedName("payph")
    @Expose
    private String payph;
    @SerializedName("infomy")
    @Expose
    private String infomy;
    @SerializedName("paymy")
    @Expose
    private String paymy;
    @SerializedName("infoid")
    @Expose
    private String infoid;
    @SerializedName("payid")
    @Expose
    private String payid;
    @SerializedName("infolv")
    @Expose
    private String infolv;
    @SerializedName("paylv")
    @Expose
    private String paylv;
    @SerializedName("paycn2")
    @Expose
    private String paycn2;
    @SerializedName("infocn2")
    @Expose
    private String infocn2;
    @SerializedName("payee")
    @Expose
    private String payee;
    @SerializedName("infoee")
    @Expose
    private String infoee;
    @SerializedName("payfr")
    @Expose
    private String payfr;
    @SerializedName("infofr")
    @Expose
    private String infofr;
    @SerializedName("payil")
    @Expose
    private String payil;
    @SerializedName("infoil")
    @Expose
    private String infoil;
    @SerializedName("payro")
    @Expose
    private String payro;
    @SerializedName("inforo")
    @Expose
    private String inforo;
    @SerializedName("payvn")
    @Expose
    private String payvn;
    @SerializedName("infovn")
    @Expose
    private String infovn;
    @SerializedName("payus")
    @Expose
    private String payus;
    @SerializedName("infous")
    @Expose
    private String infous;
    @SerializedName("paypy")
    @Expose
    private String paypy;
    @SerializedName("infopy")
    @Expose
    private String infopy;
    @SerializedName("payar")
    @Expose
    private String payar;
    @SerializedName("infoar")
    @Expose
    private String infoar;
    @SerializedName("payke")
    @Expose
    private String payke;
    @SerializedName("infoke")
    @Expose
    private String infoke;
    @SerializedName("paykg")
    @Expose
    private String paykg;
    @SerializedName("infokg")
    @Expose
    private String infokg;
    @SerializedName("paypl")
    @Expose
    private String paypl;
    @SerializedName("infopl")
    @Expose
    private String infopl;
    @SerializedName("paybr")
    @Expose
    private String paybr;
    @SerializedName("infobr")
    @Expose
    private String infobr;
    @SerializedName("infoza")
    @Expose
    private String infoza;
    @SerializedName("payza")
    @Expose
    private String payza;

    @SerializedName("infola")
    @Expose
    private String infola;
    @SerializedName("payla")
    @Expose
    private String payla;


    @SerializedName("payeg")
    @Expose
    private String payeg;
    @SerializedName("infoeg")
    @Expose
    private String infoeg;
    @SerializedName("payfl")
    @Expose
    private String payfl;
    @SerializedName("infofl")
    @Expose
    private String infofl;
    private final static long serialVersionUID = -3002092194606011579L;

    public String getInforu() {
        return inforu;
    }

    public void setInforu(String inforu) {
        this.inforu = inforu;
    }

    public String getPayru() {
        return payru;
    }

    public void setPayru(String payru) {
        this.payru = payru;
    }

    public String getInfouk() {
        return infouk;
    }

    public void setInfouk(String infouk) {
        this.infouk = infouk;
    }

    public String getPayuk() {
        return payuk;
    }

    public void setPayuk(String payuk) {
        this.payuk = payuk;
    }

    public String getInfosp() {
        return infosp;
    }

    public void setInfosp(String infosp) {
        this.infosp = infosp;
    }

    public String getPaysp() {
        return paysp;
    }

    public void setPaysp(String paysp) {
        this.paysp = paysp;
    }

    public String getInfodk() {
        return infodk;
    }

    public void setInfodk(String infodk) {
        this.infodk = infodk;
    }

    public String getPaydk() {
        return paydk;
    }

    public void setPaydk(String paydk) {
        this.paydk = paydk;
    }

    public String getInfoua() {
        return infoua;
    }

    public void setInfoua(String infoua) {
        this.infoua = infoua;
    }

    public String getPayua() {
        return payua;
    }

    public void setPayua(String payua) {
        this.payua = payua;
    }

    public String getInfoph() {
        return infoph;
    }

    public void setInfoph(String infoph) {
        this.infoph = infoph;
    }

    public String getPayph() {
        return payph;
    }

    public void setPayph(String payph) {
        this.payph = payph;
    }

    public String getInfomy() {
        return infomy;
    }

    public void setInfomy(String infomy) {
        this.infomy = infomy;
    }

    public String getPaymy() {
        return paymy;
    }

    public void setPaymy(String paymy) {
        this.paymy = paymy;
    }

    public String getInfoid() {
        return infoid;
    }

    public void setInfoid(String infoid) {
        this.infoid = infoid;
    }

    public String getPayid() {
        return payid;
    }

    public void setPayid(String payid) {
        this.payid = payid;
    }

    public String getInfolv() {
        return infolv;
    }

    public void setInfolv(String infolv) {
        this.infolv = infolv;
    }

    public String getPaylv() {
        return paylv;
    }

    public void setPaylv(String paylv) {
        this.paylv = paylv;
    }

    public String getPaycn2() {
        return paycn2;
    }

    public void setPaycn2(String paycn2) {
        this.paycn2 = paycn2;
    }

    public String getInfocn2() {
        return infocn2;
    }

    public void setInfocn2(String infocn2) {
        this.infocn2 = infocn2;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getInfoee() {
        return infoee;
    }

    public void setInfoee(String infoee) {
        this.infoee = infoee;
    }

    public String getPayfr() {
        return payfr;
    }

    public void setPayfr(String payfr) {
        this.payfr = payfr;
    }

    public String getInfofr() {
        return infofr;
    }

    public void setInfofr(String infofr) {
        this.infofr = infofr;
    }

    public String getPayil() {
        return payil;
    }

    public void setPayil(String payil) {
        this.payil = payil;
    }

    public String getInfoil() {
        return infoil;
    }

    public void setInfoil(String infoil) {
        this.infoil = infoil;
    }

    public String getPayro() {
        return payro;
    }

    public void setPayro(String payro) {
        this.payro = payro;
    }

    public String getInforo() {
        return inforo;
    }

    public void setInforo(String inforo) {
        this.inforo = inforo;
    }

    public String getPayvn() {
        return payvn;
    }

    public void setPayvn(String payvn) {
        this.payvn = payvn;
    }

    public String getInfovn() {
        return infovn;
    }

    public void setInfovn(String infovn) {
        this.infovn = infovn;
    }

    public String getPayus() {
        return payus;
    }

    public void setPayus(String payus) {
        this.payus = payus;
    }

    public String getInfous() {
        return infous;
    }

    public void setInfous(String infous) {
        this.infous = infous;
    }

    public String getPaypy() {
        return paypy;
    }

    public void setPaypy(String paypy) {
        this.paypy = paypy;
    }

    public String getInfopy() {
        return infopy;
    }

    public void setInfopy(String infopy) {
        this.infopy = infopy;
    }

    public String getPayar() {
        return payar;
    }

    public void setPayar(String payar) {
        this.payar = payar;
    }

    public String getInfoar() {
        return infoar;
    }

    public void setInfoar(String infoar) {
        this.infoar = infoar;
    }

    public String getPayke() {
        return payke;
    }

    public void setPayke(String payke) {
        this.payke = payke;
    }

    public String getInfoke() {
        return infoke;
    }

    public void setInfoke(String infoke) {
        this.infoke = infoke;
    }

    public String getPaykg() {
        return paykg;
    }

    public void setPaykg(String paykg) {
        this.paykg = paykg;
    }

    public String getInfokg() {
        return infokg;
    }

    public void setInfokg(String infokg) {
        this.infokg = infokg;
    }

    public String getPaypl() {
        return paypl;
    }

    public void setPaypl(String paypl) {
        this.paypl = paypl;
    }

    public String getInfopl() {
        return infopl;
    }

    public void setInfopl(String infopl) {
        this.infopl = infopl;
    }

    public String getPaybr() {
        return paybr;
    }

    public void setPaybr(String paybr) {
        this.paybr = paybr;
    }

    public String getInfobr() {
        return infobr;
    }

    public void setInfobr(String infobr) {
        this.infobr = infobr;
    }

    public String getInfoza() {
        return infoza;
    }

    public void setInfoza(String infoza) {
        this.infoza = infoza;
    }

    public String getPayza() {
        return payza;
    }

    public void setPayza(String payza) {
        this.payza = payza;
    }

    public String getPayeg() {
        return payeg;
    }

    public void setPayeg(String payeg) {
        this.payeg = payeg;
    }

    public String getInfoeg() {
        return infoeg;
    }

    public void setInfoeg(String infoeg) {
        this.infoeg = infoeg;
    }

    public String getPayfl() {
        return payfl;
    }

    public void setPayfl(String payfl) {
        this.payfl = payfl;
    }

    public String getInfofl() {
        return infofl;
    }

    public void setInfofl(String infofl) {
        this.infofl = infofl;
    }

    public String getInfofi() {
        return infofi;
    }

    public void setInfofi(String infofi) {
        this.infofi = infofi;
    }

    public String getPayfi() {
        return payfi;
    }

    public void setPayfi(String payfi) {
        this.payfi = payfi;
    }

    public String getInfokh() {
        return infokh;
    }

    public void setInfokh(String infokh) {
        this.infokh = infokh;
    }

    public String getPaykh() {
        return paykh;
    }

    public void setPaykh(String paykh) {
        this.paykh = paykh;
    }

    public String getInfoge() {
        return infoge;
    }

    public void setInfoge(String infoge) {
        this.infoge = infoge;
    }

    public String getPayge() {
        return payge;
    }

    public void setPayge(String payge) {
        this.payge = payge;
    }

    public String getInfoes() {
        return infoes;
    }

    public void setInfoes(String infoes) {
        this.infoes = infoes;
    }

    public String getPayes() {
        return payes;
    }

    public void setPayes(String payes) {
        this.payes = payes;
    }

    public String getInfose() {
        return infose;
    }

    public void setInfose(String infose) {
        this.infose = infose;
    }

    public String getPayse() {
        return payse;
    }

    public void setPayse(String payse) {
        this.payse = payse;
    }

    public String getInfopt() {
        return infopt;
    }

    public void setInfopt(String infopt) {
        this.infopt = infopt;
    }

    public String getPaypt() {
        return paypt;
    }

    public void setPaypt(String paypt) {
        this.paypt = paypt;
    }

    public String getInfomx() {
        return infomx;
    }

    public void setInfomx(String infomx) {
        this.infomx = infomx;
    }

    public String getPaymx() {
        return paymx;
    }

    public void setPaymx(String paymx) {
        this.paymx = paymx;
    }

    public String getInfodo() {
        return infodo;
    }

    public void setInfodo(String infodo) {
        this.infodo = infodo;
    }

    public String getPaydo() {
        return paydo;
    }

    public void setPaydo(String paydo) {
        this.paydo = paydo;
    }

    public String getInfolt() {
        return infolt;
    }

    public void setInfolt(String infolt) {
        this.infolt = infolt;
    }

    public String getPaylt() {
        return paylt;
    }

    public void setPaylt(String paylt) {
        this.paylt = paylt;
    }

    public String getInfola() {
        return infola;
    }

    public void setInfola(String infola) {
        this.infola = infola;
    }

    public String getPayla() {
        return payla;
    }

    public void setPayla(String payla) {
        this.payla = payla;
    }
}