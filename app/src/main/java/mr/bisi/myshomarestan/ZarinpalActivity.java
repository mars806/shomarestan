package mr.bisi.myshomarestan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ZarinpalActivity extends AppCompatActivity {

    public boolean continue_or_stop;
    public Handler mHandler;
    String money;
    String userID;
    WebView webView;
    ImageView imgToolbar;
    TextView textView;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zarinpal);

        textView = findViewById(R.id.txtToolbar);
        imgToolbar = findViewById(R.id.imgToolbar);
        textView.setText("درگاه پرداخت آنلاین");

        Bundle bundle = getIntent().getExtras();
        if (bundle != null)
            money = bundle.getString("money");
        else
            finish();

        if (money == null)
            finish();

        userID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        webView = findViewById(R.id.webView1);
        webView.setBackgroundColor(0);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(false);
        webView.clearCache(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.setWebViewClient(new WebViewClient());
        webView.loadUrl("http://httppdida2018.ir/shomarestan/start.php?user=" + userID + "&coin=" + money);

        mHandler = new Handler();

        continue_or_stop = true;

        new Thread(() -> {
            while (continue_or_stop) {
                try {
                    Thread.sleep(5000);
                    mHandler.post(() -> {
                        if (webView.getUrl().contains("Status=NOK")) {
                            continue_or_stop = false;
                            finish();
                        }
                        if (webView.getUrl().contains("Status=OK")) {
                            continue_or_stop = false;
                            Toast.makeText(ZarinpalActivity.this, "پرداخت با موفقیت انجام شد", Toast.LENGTH_LONG).show();
                            startActivity(new Intent(ZarinpalActivity.this, ListActivity.class));
                            System.exit(0);
                        }

                        textView.setText(Html.fromHtml("<font color='#48ce13'>https://</font><font color='#ffffff'>" + webView.getUrl().toString().toString().replace("https://", "").toString().replace("http://", "").toString() + "</font>"));
                        imgToolbar.setImageDrawable(getResources().getDrawable(R.drawable.greenlock));
                    });
                } catch (Exception e) {
                    Log.e("ZarinpalActivity", "onCreate: " + e);
                }
            }
        }).start();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 4) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage("مطمئنی میخوای خرید فعلی رو لغو کنی؟");
            dialog.setPositiveButton("اره،لغو میکنم", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            dialog.setNegativeButton("نه،ادامه میدم", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            dialog.show();
        }
        return super.onKeyDown(keyCode, event);
    }
}
