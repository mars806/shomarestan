package mr.bisi.myshomarestan;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.jetbrains.annotations.NotNull;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import mr.bisi.myshomarestan.api.APIClient;
import mr.bisi.myshomarestan.api.APIService;
import mr.bisi.myshomarestan.databinding.ActivityMainBinding;
import mr.bisi.myshomarestan.model.CodeModel;
import mr.bisi.myshomarestan.model.VerifyCodeModel;
import mr.bisi.myshomarestan.service.MyService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    CountDownTimer timer;
    APIService apiService;
    String userId;
    String opt;
    String countryID;
    String countryCode;
    String title;
    String number;

    String vNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        EventBus.getDefault().register(this);

        Intent intent = getIntent();
        countryID = intent.getStringExtra("countryId");
        opt = intent.getStringExtra("opt");
        countryCode = intent.getStringExtra("CountryCode");
        title = intent.getStringExtra("title");

        binding.txtToolbar.setText(title);

        apiService = APIClient.getRetrofitInstance().create(APIService.class);
        getCredit();

        binding.btnCopyPhoneNumber.setOnClickListener(v -> copyInClipboard(binding.editNumber));

        binding.btnCopyCode.setOnClickListener(v ->{
                ((ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE))
                        .setPrimaryClip(ClipData.newPlainText("", binding.editCode.getText()));
            Toast.makeText(MainActivity.this, "با موفقیت کپی شد", Toast.LENGTH_LONG).show();
        });

    }

    private void copyInClipboard(EditText editText) {
        ClipboardManager clipboardManager = ((ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE));
        clipboardManager.setPrimaryClip(ClipData.newPlainText("", editText.getText().toString().replace(countryCode, "")));
        Toast.makeText(MainActivity.this, "با موفقیت کپی شد", Toast.LENGTH_LONG).show();

    }


    private void getCredit() {
        userId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        getCode();
    }


    private void getCode(){
        Call<CodeModel> call = apiService.getCodeNumber(userId,  opt, "receivenumber", "1000", countryID.toLowerCase());
        call.enqueue(new Callback<CodeModel>() {
            @Override
            public void onResponse(@NotNull Call<CodeModel> call, @NotNull Response<CodeModel> response) {

                if(response.isSuccessful()){
                    Log.i("MainActivity", "onResponse: " + response.body());
                    CodeModel model = response.body();
                    if (model != null) {
                        if (model.getNumber() != null && !model.getNumber().equals("null")) {
                            hideProgressBar();
                            binding.editNumber.setText(model.getNumber());
                            binding.imageView1.setVisibility(View.VISIBLE);

                            Intent intent = new Intent(MainActivity.this, MyService.class);
                            intent.putExtra("opt", opt);
                            intent.putExtra("countryId", countryID);
                            intent.putExtra("CountryCode", countryCode);
                            intent.putExtra("tzID", model.getId());
                            intent.putExtra("title", title.replace("شماره مجازی", ""));
                            intent.putExtra("userId", userId);
                            intent.putExtra("number", model.getNumber());
                            number = model.getNumber();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                startForegroundService(intent);
                            } else {
                                startService(intent);
                            }
                        }else {
                            getCodeByDelay();
                        }
                    }else
                        getCodeByDelay();
                }else
                    getCodeByDelay();
            }

            @Override
            public void onFailure(@NotNull Call<CodeModel> call, @NotNull Throwable t) {
                hideProgressBar();
                Log.i("MainActivity", "Throwable: " + t);
                getCode();
            }
        });
    }

    private void getCodeByDelay(){
        if (timer != null) {
            timer.cancel();
        }
        timer = new CountDownTimer(10 * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                //nothing to do
            }
            public void onFinish() {
               getCode();
            }
        }.start();

    }

    private void showProgressBar(){
        binding.progressBar1.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar(){
        binding.progressBar1.setVisibility(View.GONE);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(VerifyCodeModel event) {
        binding.editCode.setText(event.getText());
        insertInfo(event.getText());
    }

    private void insertInfo(String code) {
        Call<String> call = apiService.insertInfo(userId, number, code, title.replace("شماره مجازی", ""));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NotNull Call<String> call, @NotNull Response<String> response) {
                Log.i("myService", "insertInfo onResponse: " + response.body());

            }

            @Override
            public void onFailure(@NotNull Call<String> call, @NotNull Throwable t) {
                Log.i("myService", "insertInfo Throwable: " + t);
            }
        });
    }

    @Override
    public void onBackPressed() {
        showOnBackPressedAlertDialog();
    }

    private void showOnBackPressedAlertDialog() {
        AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(
                MainActivity.this);
        alertDialog2.setTitle("خروج از صفحه");

        alertDialog2.setMessage("می خواهید خارج شوید؟");

        alertDialog2.setPositiveButton("بله", (dialog, which) -> {
            Intent intent = new Intent(MainActivity.this, ListActivity.class);
            startActivity(intent);
            finish();
        });
        alertDialog2.setNegativeButton("خیر", (dialog, which) -> dialog.cancel());

        alertDialog2.show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null)
            timer.cancel();
        EventBus.getDefault().unregister(this);
        Intent intent = new Intent(MainActivity.this, MyService.class);
        stopService(intent);
    }
}
