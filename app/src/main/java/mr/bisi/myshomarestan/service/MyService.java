package mr.bisi.myshomarestan.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import mr.bisi.myshomarestan.R;
import mr.bisi.myshomarestan.api.APIClient;
import mr.bisi.myshomarestan.api.APIService;
import mr.bisi.myshomarestan.model.VerifyCodeModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyService extends Service {

    Handler handler;
    String userId;
    String opt;
    String countryID;
    String countryCode;
    String title;
    String tzID;
    String number, code;

    APIService apiService;

    int tryNumber = 0;

    NotificationCompat.Builder builder;
    NotificationManager notificationManager;

    public MyService() {
    }

    boolean hasData = false;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        Log.i("myService", "onStartCommand: ");

        try {

            countryID = intent.getStringExtra("countryId");
            opt = intent.getStringExtra("opt");
            countryCode = intent.getStringExtra("CountryCode");
            title = intent.getStringExtra("title");
            userId = intent.getStringExtra("userId");
            tzID = intent.getStringExtra("tzID");
            number = intent.getStringExtra("number");
        }catch (Exception e){
            Toast.makeText(this, "اطلاعات موجود نیست. لطفا دوباره تلاش کنید", Toast.LENGTH_LONG).show();
            stopSelf();
        }

        startForeground(1, provideNotification(this).build());

        apiService = APIClient.getRetrofitInstance().create(APIService.class);

        handler = new Handler();
        new Thread(() -> {
            while (!hasData) {
                try {
                    tryNumber +=1;
                    Log.i("myService", "tryNumber: " + tryNumber);
                    if (tryNumber > 30)
                        stopSelf();

                    Thread.sleep(10000);
                    handler.post(() -> {
                        Call<VerifyCodeModel> call = apiService.getVerifyCode(userId,  opt, "receivepm", tzID, countryID.toLowerCase());
                        call.enqueue(new Callback<VerifyCodeModel>() {
                            @Override
                            public void onResponse(@NotNull Call<VerifyCodeModel> call, @NotNull Response<VerifyCodeModel> response) {
                                Log.i("myService", "onResponse: " + response.body());
                                if (response.isSuccessful()){
                                    VerifyCodeModel model = response.body();
                                    if (model != null) {
                                        if (model.getText() != null && !model.getText().equals("")) {
                                            EventBus.getDefault().post(model);
                                            hasData = true;
                                            code = model.getText();

                                            builder.setContentTitle("کد دریافت شد ")
                                                    .setContentText(" کد: " + code)
                                                    .build();
                                            notificationManager.notify(1, builder.build() );
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onFailure(@NotNull Call<VerifyCodeModel> call, @NotNull Throwable t) {
                                Log.i("myService", "Throwable: " + t);

                            }
                        });
                    });
                } catch (Exception e) {
                    Log.i("myService", "Exception: " + e);
                }
            }
        }).start();

        return START_NOT_STICKY;
    }



    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "reminder";
            String description = "show notification for your reminders";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("reminder", name, importance);
            channel.setDescription(description);

            notificationManager = getApplicationContext().getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

    }

    private NotificationCompat.Builder provideNotification(Context context) {
        createNotificationChannel();

        builder = new NotificationCompat.Builder(context, "reminder")
                .setContentTitle("در انتظار دریافت کد")
                .setSmallIcon(R.drawable.in_app)
                .setStyle(new NotificationCompat.BigTextStyle());

        return builder;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopForeground(true);
    }
}